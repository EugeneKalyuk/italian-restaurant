$(document).ready(function () {
    $('.nav__item').hover(function () {
        $(this).children('ul').stop(true, true).fadeIn(200).animate({top: '70px', opacity: 1}, 300);
        $(this).children('a').css({color: '#76c212', transition: 'color .5s'})

    }, function () {
        $(this).children('ul').stop(false, true).animate({top: '200px', opacity: 0}, 300).fadeOut(300);
        $(this).children('a').css({color: 'black', transition: 'color .5s'})
    });
});

$('.sub-nav__item').hover(function () {
    $(this).children('ul').stop(true, true).fadeIn(300);
    $(this).children('a').css('color', '#7cffa1')
}, function () {
    $(this).children('ul').stop(true, true).fadeOut(300);
    $(this).children('a').css('color', 'white')
});

$(window).scroll(function () {
    if(window.scrollY>=400){
        $('#nav-toggle').addClass(' navbar-fixed-top');
        $('#nav-toggle').css('backgroundColor', 'white')

    } else {
        $('#nav-toggle').removeClass(' navbar-fixed-top');
        $('#nav-toggle').css('backgroundColor', 'transparent')
    }
});